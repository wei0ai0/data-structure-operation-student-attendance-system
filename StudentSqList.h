#include "PublicActions.h"

typedef struct {
	Student *elem;
	int length; // 当前学生数
	int listsize; // 最大学生数
} SqList;  //顺序表类型定义

int Partition(SqList &L, int low, int high) {
	L.elem[0] = L.elem[low];
	int key = L.elem[low].student_id;
	while (low < high) {
		while (low < high && L.elem[high].student_id >= key)
			--high;
		L.elem[low] = L.elem[high];
		while (low < high && L.elem[low].student_id <= key)
			++low;
		L.elem[high] = L.elem[low];
	}
	L.elem[low] = L.elem[0];
	return low;
}//快速排序

void QSort(SqList &L, int low, int high) {
	if (low < high) {
		int key = Partition(L, low, high);
		QSort(L, low, key - 1);
		QSort(L, key + 1, high);
	}
}

Status InitList_Sq(SqList &L) {
	L.elem = (Student *) malloc(LIST_INIT_SIZE * sizeof(Student));
	if (!L.elem)
		return ERROR;
	L.length = 0;
	L.listsize = LIST_INIT_SIZE;
	return OK;
} // 分配空间，初始化为空表。若空间分配成功，返回OK，否则返回ERROR

Status ListEmpty(SqList L) {
	if (L.length == 0)
		return OK;
	else
		return ERROR;
} // 判断顺序表是否为空，如果表空返回OK,非空返回ERROR

Student student_input() {
	Student student;
	printf("该学生的学号：");
	scanf("%d", &student.student_id);
	printf("该学生的名字：");
	scanf("%s", &student.name);
	printf("该学生今天需要上的课数：");
	scanf("%d", &student.course_count);
	for (int i = 0; i < student.course_count; ++i) {
		printf("该学生第%d门的课：", i + 1);
		scanf("%s", &student.courses[i]);
	}
	return student;
} // 单个学信息录入

void student_print(Student student) {
	printf("学号：%d，姓名：%s，今日有%d门课，分别为：",
	       student.student_id, student.name, student.course_count);
	for (int i = 0; i < student.course_count; ++i) {
		printf("%s ", student.courses[i]);
	}
	printf("\n");
} // 单个学信息输出

void ListPrint(SqList L) {
	for (int i = 1; i <= L.length; ++i) {
		student_print(L.elem[i]);
	}
} //打印输出表L中的各个元素，即打印所有学生信息


int  LocateElem_Sq(SqList L, int e) {
	int top, bottom, mid; //定义了顺序表的两端和中间的位置
	top = L.length;
	bottom = 1;
	while (top >= bottom) {
		mid = (top + bottom) / 2; //每一次循环的mid都要改变
		if (e > L.elem[mid].student_id) {
			bottom = mid + 1; //如果他比中间位置的数要小，就在左半边
		} else if (e < L.elem[mid].student_id) {

			top = mid - 1; //如果他比中间位置的数要大，就在右半边
		} else if (e == L.elem[mid].student_id) {
			return mid;
		}
	}
    return 0;
}//折半查找

void ListDelete(SqList &L) {
	int x;
	printf("请输出要删除的学号：");
	scanf("%d", &x);
	int n = LocateElem_Sq(L, x);
	if (n < 1 || n > L.length) {
		printf("删除失败!\n");
		return;
	}
	for (int i = n; i <= L.length; i++) {
		L.elem[i] = L.elem[i + 1];
	}
	L.length--;
	printf("删除成功！\n");
} // 删除学生

void Student_Input(SqList &L) {
	InitList_Sq(L);
	int num = 0;
	FILE *fp = fopen("Student.txt", "at+");
	if (!fp) {
		printf("错误！未能打开文件\n");
		exit(0);
	}
	fscanf(fp, "%d", &num);//读入已经在系统中的学生的个数
	printf("当前系统中储存的学生个数：%d人\n", num);
	L.length = num;        //学生个数和长度对应
	for (int i = 1; i <= num; i++) {//读入系统中学生的信息
		Student stu;
		fscanf(fp, "%d", &stu.student_id);    //id
		fscanf(fp, "%s", &stu.name);//姓名
		fscanf(fp, "%d", &stu.course_count); //课程数量
		for (int j = 0; j < stu.course_count; ++j) {
			fscanf(fp, "%s", &stu.courses[j]);//课程名
		}
		L.elem[i] = stu;
	}
	fclose(fp);
	QSort(L, 1, L.length);
} // 读取文件

void Students_Print(SqList &L) {
	int i;
	FILE *fp = fopen("Student.txt", "w+");
	if (!fp) {
		printf("错误！未能打开文件\n");
		exit(0);
	}
	fprintf(fp, "%d\n", L.length);
	for (i = 1; i <= L.length; i++) {//读入系统中学生的信息
		Student stu = L.elem[i] ;
		fprintf(fp, "%d ", stu.student_id);    //id
		fprintf(fp, "%s ", stu.name);//姓名
		fprintf(fp, "%d ", stu.course_count); //课程数量
		for (int j = 0; j < stu.course_count; ++j) {
			fprintf(fp, "%s ", stu.courses[j]);//课程名
		}
		fprintf(fp, "\n");

	}
} // 最后输出文件

void insert_student(SqList &L) {
	if (L.length >= L.listsize) {  	//当线性表长度已经大于存储空间，就分配更多的空间
		Student *newbase = (Student *)realloc(L.elem,
		                                      (L.listsize + LISTINCREMENT) * sizeof(Student)); 	//每次多分配十个
		L.elem = newbase;
		L.listsize += LISTINCREMENT;
	}
	L.elem[L.length + 1] = student_input();	//直接添加到尾部
	L.length++;
	QSort(L, 1, L.length);//重新排序
	printf("添加成功！\n");
} // 添加学生

void change_student(SqList &L) {
	int x, sno;
	char name;
	printf("请输入要修改的学号：");
	scanf("%d", &x);
	int n = LocateElem_Sq(L, x);//找到修改的学号位置
	Student stu;
	stu = L.elem[n];//复制
	printf("请输入今日的课程数：");
	scanf("%d", &stu.course_count);
	for (int i = 0; i < stu.course_count; ++i) {
		printf("该学生第%d门的课：", i + 1);
		scanf("%s", &stu.courses[i]);
	}
	L.elem[n] = stu;//替换修改的部分
}//学生信息表的修改