#include "StudentSqList.h"

typedef struct QNode  //队列中的节点类型
{
    Attendance data;
    struct QNode *next;
} QNode, *QueuePtr;

typedef struct {
    char name[20];  // 课程名
    int student_number; //总人数
    int attendance_number; // 出勤人数
    QueuePtr front;
    QueuePtr rear;
} LinkQueue;   //定义连队列类型

Status InitQueue(LinkQueue &Q) {
    Q.front = (QueuePtr) malloc(sizeof(QNode));
    Q.front->next = NULL;
    Q.student_number = 0;
    Q.attendance_number = 0;
    if (!Q.front)
        return ERROR;
    Q.rear = Q.front;
    return OK;
}//初始化一个空队列，即分配头节点。头指针和尾指针初始化在头节点位置。成功返回OK，失败返回ERROR。

Status GetHead(LinkQueue Q, Attendance &e) {
    if (Q.rear == Q.front)
        return ERROR;
    e = Q.front->next->data;
    return OK;
}//获取队头元素的值并返回在e中，取值成功返回OK，失败返回ERROR

Status EnQueue(LinkQueue &Q, Attendance e) {
    QueuePtr p = (QueuePtr) malloc(sizeof(QNode));
    if (!p)
        return ERROR;
    p->data = e;
    p->next = NULL;
    Q.rear->next = p;
    Q.rear = p;
    Q.attendance_number++;
    return OK;
}//将元素e入队成为新的队尾元素

Status DeQueue(LinkQueue &Q, Attendance &e) {
    if (Q.rear == Q.front)
        return ERROR;
    QueuePtr p = Q.front->next;
    e = p->data;
    Q.front->next = p->next;
    if (Q.rear == p)
        Q.rear = Q.front;
    free(p);
    Q.attendance_number--;
    return OK;
}//将队头元素出队 ，其值返回在e中。出队成功返回OK，失败返回ERROR。

int QueueLength(LinkQueue Q) {
    QueuePtr p = Q.front;
    int i = 0;
    while (p != Q.rear) {
        p = p->next;
        i++;
    }
    return i;
}//求队列长度操作，即返回实际元素个数。

int Course_Input(LinkQueue queue_courses[], int MAX_COURSE) {
    FILE *fp = fopen("Course.txt", "at+");
    if (!fp) {
        printf("错误！未能打开文件\n");
        exit(0);
    }
    int m = 0;
    fscanf(fp, "%d", &m);
    printf("今日学校安排课程数：%d门\n", m);
    for (int i = 0; i < m; i++) {
        InitQueue(queue_courses[i]);
        fscanf(fp, "%s", &queue_courses[i].name);
        fscanf(fp, "%d", &queue_courses[i].student_number);
    }
    for (int i = m; i < MAX_COURSE; ++i) {
        InitQueue(queue_courses[i]);
    }
    fclose(fp);
    return m; // 返回课程数，最后要用于再次输出
} // 创建课程队列数组

void course_print(LinkQueue queue_course) {
    printf("课程名：%s，总人数：%d\n", queue_course.name, queue_course.student_number);
} // 输出单个课程信息

void courses_print(LinkQueue queue_courses[], int MAX_COURSE) {
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (queue_courses[i].student_number) {
            course_print(queue_courses[i]);
        }
    }
} // 输出所有课程信息

void Courses_Print(LinkQueue queue_courses[], int MAX_COURSE, int course_number) {
    FILE *fp = fopen("Course.txt", "w+");
    if (!fp) {
        printf("错误！未能打开文件\n");
        exit(0);
    }
    fprintf(fp, "%d\n", course_number);
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (queue_courses[i].student_number != 0) {
            fprintf(fp, "%s %d\n", queue_courses[i].name, queue_courses[i].student_number);
        }
    }
    fclose(fp);
} // 最后输出文件

int insert_student(LinkQueue queue_courses[], int MAX_COURSE, int course_number) {
    int i;
    for (i = 0; i < MAX_COURSE; ++i) {
        if (queue_courses[i].student_number == 0) {
            printf("课程名：");
            scanf("%s", &queue_courses[i].name);
            printf("学生数：");
            scanf("%d", &queue_courses[i].student_number);
            course_number++;
            printf("添加成功！\n");
            break;
        }
    }
    if (i == MAX_COURSE) {
        printf("课程已满，无法添加！\n");
        return MAX_COURSE;
    }
    return course_number;
} // 添加课程，是对队列数组操作

int delete_course(LinkQueue queue_courses[], int MAX_COURSE, int course_number) {
    char name[20];
    printf("请输入要删除的课程名：");
    scanf("%s", &name);
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (strcmp(queue_courses[i].name, name) == 0) {
            queue_courses[i].student_number = 0;
            printf("删除成功！\n");
            course_number--;
            return course_number;
        }
    }
    printf("没有查找到该课程，删除失败！\n");
    return course_number;
} // 删除课程，是对队列数组操作

void change_course(LinkQueue queue_courses[], int MAX_COURSE) {
    char name[20];
    printf("请输入要修改的课程名：");
    scanf("%s", &name);
    int n;
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (strcmp(queue_courses[i].name, name) == 0) {
            printf("请输入修改后的人数：");
            scanf("%d", &n);
            queue_courses[i].student_number = n;
            printf("修改成功！\n");
            return;
        }
    }
    printf("没有查找到该课程，修改失败！\n");
} // 修改课程，是对队列数组操作

int stack_course_judgment(LinkQueue queue_courses[], char course[]) {
    for (int i = 0; i < COURSE_MAX; ++i) {
        if (strcmp(queue_courses[i].name, course) == 0)
            return i;
    }
    return -1;
} // 判断是那个课程
