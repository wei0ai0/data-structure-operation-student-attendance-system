#include "CourseTreeList.h"

#define MAX_COURSE 10 // 学校最多安排的门数

int main() {
    printf("=================================================================================\n\n");
    printf("                                大学生考勤系统\n\n");
    printf("=================================================================================\n\n");
    int n;
    int course_number; // 课程数
    int attendance_number; // 考勤数
    SqList L_student;
    LinkQueue queue_courses[MAX_COURSE];
    BiTree tree_courses[MAX_COURSE];
    Student_Input(L_student);
    course_number = Course_Input(queue_courses, MAX_COURSE);
    attendance_number = Attendance_Input(tree_courses, queue_courses, MAX_COURSE);
    while (true) {
        main_menu();
        printf("请输出你的选择：");
        scanf("%d", &n);
        if (n == 1) {
            printf("-----今日课程的输出-----\n");
            courses_print(queue_courses, MAX_COURSE);
        } else if (n == 2) {
            printf("-----今日课程的添加-----\n");
            course_number = insert_student(queue_courses, MAX_COURSE, course_number);
        } else if (n == 3) {
            printf("-----今日课程的删除-----\n");
            course_number = delete_course(queue_courses, MAX_COURSE, course_number);
        } else if (n == 4) {
            printf("-----今日课程的修改-----\n");
            change_course(queue_courses, MAX_COURSE);
        } else if (n == 5) {
            printf("-----学生信息表的输出-----\n");
            ListPrint(L_student);
        } else if (n == 6) {
            printf("-----学生信息表的添加-----\n");
            insert_student(L_student);
        } else if (n == 7) {
            printf("-----学生信息表的删除-----\n");
            ListDelete(L_student);
        } else if (n == 8) {
            printf("-----学生信息表的修改-----\n");
            change_student(L_student);
        } else if (n == 9) {
            printf("-----查询某人某课程的出勤状态-----\n");
            find_student_course_attendance_print(
                    L_student, tree_courses, queue_courses, MAX_COURSE, attendance_number);
        } else if (n == 10) {
            printf("-----添加某人某课程的出勤信息-----\n");
            add_attendance(tree_courses, queue_courses, MAX_COURSE, attendance_number);
        } else if (n == 11) {
            printf("-----查询某课程所有学生的出勤状态-----\n");
            find_course_attendance(tree_courses, queue_courses, MAX_COURSE);
        } else if (n == 12) {
            printf("-----查询某学生所有课程的出勤状态-----\n");
            find_student_courses_attendance(L_student, tree_courses, queue_courses, MAX_COURSE, attendance_number);
        } else if (n == 13) {
            printf("-----打印全部课程所有学生的出勤状态-----\n");
            courses_attendance_student(tree_courses, queue_courses, MAX_COURSE);
        } else if (n == 14) {
            printf("-----打印全部学生所有课程的出勤状态-----\n");
            students_attendance_courses(L_student, tree_courses, queue_courses, MAX_COURSE, attendance_number);
        } else if (n == 0) {
            courses_attendance_student_txt(tree_courses, queue_courses, MAX_COURSE);
            students_attendance_courses_txt(L_student, tree_courses, queue_courses, MAX_COURSE, attendance_number);
            Students_Print(L_student);
            Courses_Print(queue_courses, MAX_COURSE, course_number);
            Attendance_Print(queue_courses, MAX_COURSE, attendance_number);
            printf("谢谢使用！\n");
            break;
        }
    }
}