// 公共操作
#include "Data.h"

char *time_judgment(Attendance attendance) {
	char *str;
	str = (char *) malloc(10);
	if (attendance.attendance_time.hour == 0 && attendance.attendance_time.minute == 0)
		strcpy(str, "旷课");
	else if (attendance.attendance_time.hour == 24 && attendance.attendance_time.minute == 24)
		strcpy(str, "请假");
	else if (attendance.attendance_time.hour < attendance.class_time.hour
	         || attendance.attendance_time.hour == attendance.class_time.hour
	         && attendance.attendance_time.minute <= attendance.class_time.minute)
		strcpy(str, "准时");
	else
		strcpy(str, "迟到");
	return str;
} // 判断考勤状态

my_time time_input() {
	my_time time;
	scanf("%d:%d", &time.hour, &time.minute);
	return time;
} // 时间的输入

void student_attendance_print(Attendance attendance) {
	printf("课程：%s，上课时间：%02d:%02d，签到时间：%02d:%02d，签到状态：%s\n",
	       attendance.course, attendance.class_time.hour, attendance.class_time.minute,
	       attendance.attendance_time.hour, attendance.attendance_time.minute,
	       attendance.attendance_state);
} // 学生——单条考勤信息的输出，控制台

void student_attendance_print_txt(Attendance attendance, FILE *fp) {
	fprintf(fp, "课程：%s，上课时间：%02d:%02d，签到时间：%02d:%02d，签到状态：%s\n",
	        attendance.course, attendance.class_time.hour, attendance.class_time.minute,
	        attendance.attendance_time.hour, attendance.attendance_time.minute,
	        attendance.attendance_state);
} // 学生——单条考勤信息的输出，文件

void course_attendance_print(Attendance attendance) {
	printf("学号：%d，姓名：%s，签到时间：%02d:%02d，签到状态：%s\n",
	       attendance.student_id, attendance.name, attendance.attendance_time.hour,
	       attendance.attendance_time.minute, attendance.attendance_state);
} // 课程——单条考勤信息的输出，控制台

void course_attendance_print_txt(Attendance attendance, FILE *fp) {
	fprintf(fp, "学号：%d，姓名：%s，签到时间：%02d:%02d，签到状态：%s\n",
	        attendance.student_id, attendance.name, attendance.attendance_time.hour,
	        attendance.attendance_time.minute, attendance.attendance_state);
} // 课程——单条考勤信息的输出，文件

Attendance attendance_input() {
	Attendance attendance;
	printf("学号：");
	scanf("%d", &attendance.student_id);
	printf("姓名：");
	scanf("%s", &attendance.name);
	printf("课程：");
	scanf("%s", &attendance.course);
	printf("上课时间：");
	attendance.class_time = time_input();
	printf("签到时间：");
	attendance.attendance_time = time_input();
	strcpy(attendance.attendance_state, time_judgment(attendance));
	return attendance;
} // 输入学生考勤信息考勤

void main_menu() {
	printf("-----------------------------------------主菜单-----------------------------------------\n");
	printf("<1> 今日课程的输出          <2> 今日课程的添加             <3> 今日课程的删除     \n");
	printf("<4> 今日课程的修改          <5> 学生信息表的输出           <6> 学生信息表的添加 \n");
	printf("<7> 学生信息表的删除        <8> 学生信息表的修改           <9> 查询某人某课程的出勤状态 \n");
	printf("<10> 添加某人某课程的出勤信息\n");
	printf("<11> 查询某课程所有学生的出勤状态\t");
	printf("<12> 查询某学生所有课程的出勤状态\n");
	printf("<13> 打印全部课程所有学生的出勤状态\t");
	printf("<14> 打印全部学生所有课程的出勤状态\n");
	printf("提示：输出\"0\"为退出程序\n");
	printf("-----------------------------------------------------------------------------------------\n");
}

void attendance_print_txt(Attendance attendance, FILE *fp) {
	fprintf(fp, "%d %s %s %02d:%02d %02d:%02d\n",
	        attendance.student_id, attendance.name, attendance.course,
	        attendance.class_time.hour, attendance.class_time.minute,
	        attendance.attendance_time.hour, attendance.attendance_time.minute);
} // 考勤信息的输出，用于更新文件