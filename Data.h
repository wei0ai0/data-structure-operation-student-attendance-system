// 公共类型
#include <stdio.h>
#include "stdlib.h"
#include <malloc.h>
#include "string.h"

#define OK 1
#define ERROR 0
#define COURSE_MAX 5  // 今日课程最多门
#define LIST_INIT_SIZE 100 //最大学生数
#define LISTINCREMENT 10 // 追加学生数
typedef int Status;

typedef struct my_time {
    int hour; // 时
    int minute; // 分
} my_time; // 时间信息

typedef struct Student {
    int student_id; // 学号
    char name[20]; // 姓名
    int course_count; // 今日课数
    char courses[COURSE_MAX][20]; // 今日课程
} Student; // 学生信息

typedef struct Attendance {
    int student_id; // 学号
    char name[20]; // 姓名
    char course[20]; // 课程名
    my_time class_time; // 上课时间
    my_time attendance_time; // 签到时间
    char attendance_state[10]; // 考勤状态
} Attendance; // 学生考勤信息
