#include "CourseLinkQueue.h"

typedef struct BiTNode {
    Attendance data;
    struct BiTNode *lchild, *rchild;
} BiTNode, *BiTree;  //二叉排序树定义

Status Searchbst(BiTree T, int key, BiTree f, BiTree &p) {
    if (!T) {
        p = f;
        return ERROR;
    } else if (key == T->data.student_id) {
        p = T;
        return OK;
    } else if (key < T->data.student_id) {
        return Searchbst(T->lchild, key, T, p);
    } else {
        return Searchbst(T->rchild, key, T, p);
    }
}//在二叉排序树T中查找关键字key，查找成功返回OK，失败返回ERROR；

void Insertree(BiTree &T, Attendance e) {
    BiTree p;
    if (!Searchbst(T, e.student_id, NULL, p)) {
        BiTree s = (BiTree) malloc(sizeof(BiTNode));
        s->data = e;
        s->lchild = s->rchild = NULL;
        if (!p) {
            T = s;
        } else if (e.student_id < p->data.student_id) {
            p->lchild = s;
        } else {
            p->rchild = s;
        }
    }
} //在二叉排序树T中插入关键字e

void InOrderTraverse_course(BiTree T) {
    if (T) {
        InOrderTraverse_course(T->lchild);
        course_attendance_print(T->data);
        InOrderTraverse_course(T->rchild);
    }
}//对二叉排序树T进行中序遍历

void InOrderTraverse_course_txt(BiTree T,FILE *fp) {
    if (T) {
        InOrderTraverse_course_txt(T->lchild,fp);
        course_attendance_print_txt(T->data,fp);
        InOrderTraverse_course_txt(T->rchild,fp);
    }
}//对二叉排序树T进行中序遍历，文件输出

int Attendance_Input(BiTree tree_courses[], LinkQueue queue_courses[], int MAX_COURSE) {
    FILE *fp = fopen("Attendance.txt", "at+");
    if (!fp) {
        printf("错误！未能打开文件\n");
        exit(0);
    }
    for (int i = 0; i < MAX_COURSE; ++i) {
        tree_courses[i] = NULL;
    }
    int m = 0;
    int s;
    Attendance attendance;
    fscanf(fp, "%d", &m);
    printf("今日考勤总数：%d条\n", m);
    for (int i = 0; i < m; ++i) {
        fscanf(fp, "%d", &attendance.student_id);
        fscanf(fp, "%s", &attendance.name);
        fscanf(fp, "%s", &attendance.course);
        fscanf(fp, "%d:%d", &attendance.class_time.hour, &attendance.class_time.minute);
        fscanf(fp, "%d:%d", &attendance.attendance_time.hour, &attendance.attendance_time.minute);
        strcpy(attendance.attendance_state, time_judgment(attendance));
        s = stack_course_judgment(queue_courses, attendance.course);
        EnQueue(queue_courses[s], attendance);
        Insertree(tree_courses[s], attendance);
    }
    return m;
} // 读取考勤信息

void courses_attendance_student(BiTree tree_courses[], LinkQueue queue_courses[], int MAX_COURSE) {
    Attendance attendance;
    int f;
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (queue_courses[i].student_number != 0) {
            f = GetHead(queue_courses[i], attendance);
            if (f == ERROR)
                continue;
            printf("课程名：%s，签到时间：%02d:%02d\n", attendance.course,
                   attendance.class_time.hour, attendance.class_time.minute);
            InOrderTraverse_course(tree_courses[i]);
            printf("\n");
        }
    }
}//打印全部课程所有学生的出勤状态

void courses_attendance_student_txt(BiTree tree_courses[], LinkQueue queue_courses[], int MAX_COURSE) {
    Attendance attendance;
    FILE *fp = fopen("CoursesStudent.txt", "w+");
    if (!fp) {
        printf("错误！未能打开文件\n");
        exit(0);
    }
    int f;
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (queue_courses[i].student_number != 0) {
            f = GetHead(queue_courses[i], attendance);
            if (f == ERROR)
                continue;
            fprintf(fp,"课程名：%s，签到时间：%02d:%02d\n", attendance.course,
                    attendance.class_time.hour, attendance.class_time.minute);
            InOrderTraverse_course_txt(tree_courses[i],fp);
            fprintf(fp,"\n");
        }
    }
    fclose(fp);
}//打印全部课程所有学生的出勤状态 -- 文件

void find_course_attendance(BiTree tree_courses[], LinkQueue queue_courses[], int MAX_COURSE) {
    char name[20];
    int f;
    Attendance attendance;
    printf("请输入查找的课程名：");
    scanf("%s", &name);
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (strcmp(queue_courses[i].name, name) == 0) {
            f = GetHead(queue_courses[i], attendance);
            if (f == ERROR) {
                printf("该课程无人签到！\n");
                return;
            }
            printf("签到时间：%02d:%02d\n", attendance.class_time.hour, attendance.class_time.minute);
            InOrderTraverse_course(tree_courses[i]);
            return;
        }
    }
    printf("该课程的无效课程，无法进行查询\n");
} //查询某课程所有学生的出勤状态

Status find_student_course_attendance(
        SqList L_student,BiTree tree_courses[], LinkQueue queue_courses[],
        int MAX_COURSE, char *course, int student_id,BiTree &p,int &attendance_number) {
    int f = 0;
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (strcmp(queue_courses[i].name, course) == 0) {
            f = Searchbst(tree_courses[i], student_id, NULL, p);
            if(f == OK) {
                return OK;
            }
            else{
                // 查找失败，认为旷课，添加旷课的考勤信息
                Attendance attendance,e;
                f = GetHead(queue_courses[i],e);
                if(f == ERROR)
                    return 3;// 该课程里面一个签到的学生没有，为无效课
                int x = LocateElem_Sq(L_student,student_id);
                if( x == 0){
                    return 4;
                }
                attendance.student_id = student_id;
                strcpy(attendance.name,L_student.elem[x].name);
                strcpy(attendance.course,course);
                attendance.class_time = e.class_time;
                attendance.attendance_time.hour =0;
                attendance.attendance_time.minute =0;
                strcpy(attendance.attendance_state,"旷课");
                EnQueue(queue_courses[i],attendance);
                queue_courses[i].attendance_number++;
                Insertree(tree_courses[i],attendance);
                attendance_number++;
                p->data = attendance;
                return ERROR;
            }
        }
    }
    return 3;
} // 查询某人某课程的出勤状态，如果查找失败，认为旷课，添加旷课的考勤信息

void find_student_course_attendance_print(
        SqList L_student,BiTree tree_courses[], LinkQueue queue_courses[],int MAX_COURSE,int &attendance_number){
    char course[20];
    int student_id;
    BiTree p;
    int f=0;
    printf("请输出查询的课程：");
    scanf("%s",&course);
    printf("请输出查询的学号：");
    scanf("%d",&student_id);
    f = find_student_course_attendance(
            L_student,tree_courses,queue_courses,MAX_COURSE,course,student_id,p,attendance_number);
    if(f==ERROR){
        find_student_course_attendance(
                L_student,tree_courses,queue_courses,MAX_COURSE,course,student_id,p,attendance_number);
    } else if(f==3){
        printf("该课程的无效课程，无法进行查询！\n");
        return;
    } else if(f==4){
        printf("没有该学生，无法进行查询！\n");
        return;
    }
    printf("该课程该学生的出勤状态：%s\n",p->data.attendance_state);
} // 输出查询某人某课程的出勤状态

void find_student_courses_attendance(
        SqList L_student,BiTree tree_courses[], LinkQueue queue_courses[],
        int MAX_COURSE,int &attendance_number){
    int student_id;
    printf("请输出需要查询的学号：");
    scanf("%d",&student_id);
    int x = LocateElem_Sq(L_student,student_id);
    if(x==0){
        printf("没有该学生，无法进行查询！\n");
        return;
    }
    BiTree p;
    printf("姓名：%s，课程：",L_student.elem[x].name);
    for (int i = 0; i < L_student.elem[x].course_count; ++i) {
        printf("%s(",L_student.elem[x].courses[i]);
        find_student_course_attendance(
                L_student,tree_courses,queue_courses,MAX_COURSE,L_student.elem[x].courses[i],
                L_student.elem[x].student_id,p,attendance_number);
        printf("%s) ",p->data.attendance_state);
    }
    printf("\n");
} //查询某学生所有课程的出勤状态

void students_attendance_courses(
        SqList L_student,BiTree tree_courses[], LinkQueue queue_courses[],
        int MAX_COURSE,int &attendance_number){
    for (int i = 1; i <= L_student.length; ++i) {
        BiTree p;
        printf("学号：%d,姓名：%s，课程：",L_student.elem[i].student_id,L_student.elem[i].name);
        for (int j = 0; j < L_student.elem[i].course_count; ++j) {
            printf("%s(",L_student.elem[i].courses[j]);
            find_student_course_attendance(
                    L_student,tree_courses,queue_courses,MAX_COURSE,L_student.elem[i].courses[j],
                    L_student.elem[i].student_id,p,attendance_number);
            printf("%s) ",p->data.attendance_state);
        }
        printf("\n");
    }
} // 打印全部学生所有课程的出勤状态

void add_attendance(BiTree tree_courses[], LinkQueue queue_courses[],
                    int MAX_COURSE,int &attendance_number){
    Attendance attendance;
    attendance = attendance_input();
    int n = stack_course_judgment(queue_courses,attendance.course);
    if(n!=-1){
        EnQueue(queue_courses[n],attendance);
        queue_courses[n].attendance_number++;
        Insertree(tree_courses[n],attendance);
        attendance_number++;
        printf("添加成功！\n");
    } else{
        printf("没有查询的相应课程，添加失败！\n");
    }
} //添加某人某课程的出勤信息

void students_attendance_courses_txt(
        SqList L_student,BiTree tree_courses[], LinkQueue queue_courses[],
        int MAX_COURSE,int &attendance_number){
    FILE *fp = fopen("StudentCourses.txt", "w+");
    if (!fp) {
        printf("错误！未能打开文件\n");
        exit(0);
    }
    for (int i = 1; i <= L_student.length; ++i) {
        BiTree p;
        fprintf(fp,"学号：%d,姓名：%s，课程：",L_student.elem[i].student_id,L_student.elem[i].name);
        for (int j = 0; j < L_student.elem[i].course_count; ++j) {
            fprintf(fp,"%s(",L_student.elem[i].courses[j]);
            find_student_course_attendance(
                    L_student,tree_courses,queue_courses,MAX_COURSE,L_student.elem[i].courses[j],
                    L_student.elem[i].student_id,p,attendance_number);
            fprintf(fp,"%s) ",p->data.attendance_state);
        }
        fprintf(fp,"\n");
    }
    fclose(fp);
} // 打印全部学生所有课程的出勤状态 -- 文件

void Attendance_Print(LinkQueue queue_courses[], int MAX_COURSE, int attendance_number) {
    FILE *fp = fopen("Attendance.txt", "w+");
    if (!fp) {
        printf("错误！未能打开文件\n");
        exit(0);
    }
    int f = 0;
    Attendance attendance;
    fprintf(fp, "%d\n", attendance_number);
    for (int i = 0; i < MAX_COURSE; ++i) {
        if (queue_courses[i].student_number != 0) {
            f = GetHead(queue_courses[i], attendance);
            while (f == OK) {
                attendance_print_txt(attendance, fp);
                f = DeQueue(queue_courses[i], attendance);
            }
        }
    }
    fclose(fp);
} // 考勤信息的输出文件